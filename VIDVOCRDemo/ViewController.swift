//
//  ViewController.swift
//  VIDVOCRDemo
//
//  Created by Mina Atef on 31/01/2022.
// ocr

import UIKit
import VIDVOCR
class ViewController: UIViewController {
    @IBOutlet weak var startBtn: UIButton!
            
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        startBtn.alpha = 1
        startBtn.isEnabled = true
    }
    
    @IBAction func startPressed(_ sender: Any) {
        startBtn.alpha = 0.8
        startBtn.isEnabled = false
        
        fatalError("Please fill in the credentials before starting the SDK and then remove this fatal error line")
        let creds = Credentials(
            bundle: "",
            userName: "",
            password: "",
            clientID: "",
            clientSecret: "",
            baseURL: ""
        )
        
//        get the ssl certificate from your files
//        let url = Bundle(for: ViewController.self).url(forResource: "cert", withExtension: "der")
//        var certificateData = Data()
//        do {
//            certificateData = try Data(contentsOf: url!)
//        } catch {
//            return
//        }
        
        TokenGeneration.getToken(creds: creds) { token in
            DispatchQueue.main.async {[weak self] in
                guard let self = self else {return}
                var builder = OCRBuilder()
                builder = builder.setDataValidation(validate: true )
                    .setDocumentVerification(verify: true )
                    .setlLanguage(language: "en")
                    .setBundleKey(creds.bundle)
                    .setBaseUrl(creds.baseURL)
                    .setAccessToken(token ?? "")
                    .setReviewData(review: true)
                    .setCaptureOnlyMode(false)
                    .setReturnValidationError(false)
//                builder = builder.setSSLCertificate(certificateData) // (optional) ssl certificate in Data type.
                builder = builder.setPrimaryColor(color: .orange)
                builder = builder.setHeaders(headers: ["key": "value", "key2": "value2"])
                builder.start(vc: self, ocrDelegate: self)
            }
        }
    }
    
    func printResponse(data: VIDVOCRResult?) {
        let frontImageString = data?.captures?.nationalIdFront ?? ""
        let backImageString = data?.captures?.nationalIdBack ?? ""
        print("sessionId:", data?.sessionID ?? "")
        print("Document verification", data?.documentVerificationResult?.success)
        print("firstName", data?.ocrResult?.firstName ?? "")
        print("fullName", data?.ocrResult?.fullName ?? "")
        print("religion", data?.ocrResult?.religion ?? "")
        print("gender", data?.ocrResult?.gender ?? "")
        print("dateOfBirth", data?.ocrResult?.dateOfBirth ?? "")
        print("maritalStatus", data?.ocrResult?.maritalStatus ?? "")
        print("husbandName", data?.ocrResult?.husbandName ?? "")
        print("street", data?.ocrResult?.street ?? "")
        print("profession", data?.ocrResult?.profession ?? "")
        print("releaseDate", data?.ocrResult?.releaseDate ?? "")
        print("expiryDate", data?.ocrResult?.expiryDate ?? "")
        print("expired", data?.ocrResult?.expired)
        print("frontNid", data?.ocrResult?.frontNid ?? "")
        print("backNid", data?.ocrResult?.backNid ?? "")
        print("serialNumber", data?.ocrResult?.serialNumber ?? "")
        print("age", data?.ocrResult?.age ?? -500)
        print("frontDataValidity", data?.ocrResult?.frontDataValidity ?? "")
        print("backDataValidity", data?.ocrResult?.backDataValidity ?? "")
        print("isRootOfTrustCompatible", data?.ocrResult?.isRootOfTrustCompatible  ?? "")
        print("frontTransactionID", data?.ocrResult?.frontTransactionID  ?? "")
        print("backTransactionID", data?.ocrResult?.backTransactionID ?? "")
        print("combinationID", data?.ocrResult?.combinationID ?? "")
        print("governorate", data?.ocrResult?.governorate ?? "")
        print("policeStation", data?.ocrResult?.policeStation ?? "")
        print("backConfidence", data?.ocrResult?.backConfidence ?? "")
        print("frontConfidence", data?.ocrResult?.frontConfidence ?? "")
        print("birthGovernorate", data?.ocrResult?.birthGovernorate ?? "")
    }
    
}

extension ViewController: VIDVOCRDelegate {
    func onOCRResult(result: VIDVOCRResponse) {
        DispatchQueue.main.async {[weak self] in
            guard let self = self else {return}
            self.startBtn.alpha = 1
            self.startBtn.isEnabled = true
        }
        switch result {
        case .success(let data):
            printResponse(data: data)
        case .builderError(let code, let message):
            print("Builder Error", message, "code:", code)
        case .serviceFailure(let code, let message, let data):
            printResponse(data: data)
            print("Service Failure", message, "Error Code:", code)
        case .userExit(let step, let data):
            print("User Exit", step, "data: ", data)
            printResponse(data: data)
        case .capturedImages(capturedImageData: let capturedImageData):
            // fire when each image is captured
            break
        }
    }
}

struct Credentials {
    var bundle: String
    var userName: String
    var password: String
    var clientID: String
    var clientSecret: String
    var baseURL: String
}
