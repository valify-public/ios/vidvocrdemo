//
//  TokenGeneration.swift
//  VIDVOCRDemo
//
//  Created by Mina Atef on 21/03/2023.
//

import Foundation
class TokenGeneration {
    static func getToken(creds: Credentials, completion: @escaping (_ token: String?) -> Void) {
        
        let baseURL = creds.baseURL
        let userName = creds.userName
        let password = creds.password
        let clientID = creds.clientID
        let clientSecret = creds.clientSecret
        let header = "\(baseURL)/api/o/token/"
        
        var request  = URLRequest(url: URL(string: header)!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let body = "username=\(userName)&password=\(password)&client_id=\(clientID)&client_secret=\(clientSecret)&grant_type=password".data(using: String.Encoding.ascii, allowLossyConversion: false)
        
        request.httpBody = body
        
        let task = URLSession.shared.dataTask(with: request) { data, response, _ in
            do {
                
                let httpResponse = response as? HTTPURLResponse
                let statusCode = httpResponse?.statusCode
                print(httpResponse)
                guard let resdata = data else {
                    return
                }
                
                guard let jsonResponse = try JSONSerialization.jsonObject(with: (resdata), options: JSONSerialization.ReadingOptions()) as? NSDictionary else {
                    return
                }
                print(jsonResponse)
                guard let token = jsonResponse.value(forKey: "access_token") as? String else {
                    return
                }
                
                completion(token)
            } catch _ {
                print("not good JSON formatted response")
            }
        }
        task.resume()
    }
}
